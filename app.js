var express = require('express');
var bodyparser = require('body-parser');
var metier = require('./metierCarnet');

var app = express();
app.use(bodyparser.json());

app.all('*', function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*"); res.header("Access-Control-Allow-Headers",
        "Origin, X-Requested-With, Content-Type, Accept"); res.header("Access-Control-Allow-Methods",
        "GET, POST, PUT, DELETE, OPTIONS"); next();
});


// ajouter client
app.post('/api/personnes', function(req,res) {

    // Récupérer paramètres
    var personne = req.body;

    // metier
    var objres = metier.ajouter(personne);
    console.log(objres);
    // forger resultat
    if ((typeof objres === 'undefined') || (objres === {}))
    {
        res.status(400).json({});}
    else res.status(201).json(objres);
});

// lister les clients
app.get('/api/personnes', function(req,res) {
    res.status(200).json(metier.lister());
});

// Rechercher
app.get('/api/personnes/:id', function(req,res) {
    // 1
    var id = req.params.id;

    // 2
    var objres = metier.getPersonne(id);

    // 3
    if ((typeof objres === 'undefined') || (objres === {}))
        res.status(404).json({});
    else res.status(200).json(objres);
});

app.listen(3000, function() {
    console.log('Server running...')
})
