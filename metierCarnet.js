// partie Métier du Carnet (back)

// liste des personnes
var liste = [];
var id = 0;

// Constructeur TOUS membres
function Personne(id,nom,prenom,adresse,codepostal,ville) {
    this.id = id;
    this.nom = nom;
    this.prenom = prenom;
    this.adresse = adresse;
    this.codepostal = codepostal;
    this.ville = ville;
    this.datemiseajour = new Date();
}

// Constructeur avec structure
function Personne(personne) {
    this.id = personne.id;
    this.nom = personne.nom;
    this.prenom = personne.prenom;
    this.adresse = personne.adresse;
    this.codepostal = personne.codepostal;
    this.ville = personne.ville;
    this.datemiseajour = new Date();
}

// METHODES METIER

// Ajout
var ajouter = function(personne) {
    personne.id = id;
    liste[id] = new Personne(personne);
    id++;
    return liste[id-1];
}


// Get 1 personne
var getPersonne = function (i) {
    if (typeof liste[i] === 'undefined') return {};
    else return liste[i];
}

// lister les personnes
var lister = function () {
    return Object.values(liste);
}

exports.ajouter = ajouter;
exports.getPersonne = getPersonne;
exports.lister = lister;

